class Animatronic 
{
   private byte[] avaCams;
   private byte[] diff;
   private bool foxy;

   public Animatronic(byte[] avaCams, byte[] diff, bool foxy) {
      this.avaCams = avaCams; 
      this.diff = diff;
      this.foxy = foxy;
   }
}
